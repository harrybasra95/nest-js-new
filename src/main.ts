import { Controller, Get, Module } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

@Controller()
class AppController {
     @Get()
     getRootRoute() {
          return 'Hello';
     }
}

@Module({
     controllers: [AppController],
})
class AppModule {}

async function bootstrap() {
     const app = NestFactory.create(AppModule);
     await (await app).listen(3000);
}

bootstrap();
